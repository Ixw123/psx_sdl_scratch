#ifndef _COLLISION_H_INCLUDED_
#define _COLLISION_H_INCLUDED_

#include "SDL.h"

int isCollision (SDL_Rect *rect1, SDL_Rect *rect2);

#endif
