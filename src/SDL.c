#include "PSX_SDL.h"

static unsigned int prim_list[0x4000];
volatile int display_is_old = 1;
static volatile int time_counter = 0;
int dbuf = 0;

#define drawPixel16(x, y, color) \
    screen_buf[((y)*S_WIDTH+(x))] = (color);

// 0x1111111111111111
//        11111
#define RGB16(r,g,b) \
    ((unsigned short)( ((b) << 11) | ((g) << 6) | (r)))

static void prog_vblank_handler() {
    display_is_old = 1;
    time_counter++;
}

/* Use as little dynamic memory as possible... */
static SDL_Surface PSX_screensurface;
static SDL_PixelFormat PSX_pixelformat;
SDL_Joystick PSX_joystick;

/* Keep track of screen width and height (320 or 640 width, 240 or 480 height) */
int S_WIDTH = 320;
int S_HEIGHT = 240;

/*
 * Keep track of the surface returned by SDL_SetVideoMode
 */
SDL_Surface *sdl_video_surface = NULL;

int SDL_Init(Uint32 flags)
{
    /* Initialize the joystick if given */
    if (flags & SDL_INIT_JOYSTICK) {
        
        printf("Initializing joystick!\n");
        
        /* Setting joystick vals... */
        PSX_joystick.index = 0;
        PSX_joystick.name = "PS1 Controller 1";
        PSX_joystick.naxes = 2;
        PSX_joystick.axes = NULL; // null for now
        PSX_joystick.nhats = 1; // 1 hat (directionals)
        PSX_joystick.nballs = 0; // trackballs on the joystick
        PSX_joystick.balls = NULL; // no balls
        PSX_joystick.nbuttons = 10; // shapes, r1,l1,r2,l2,select,start
        PSX_joystick.buttons = NULL; // NULL for now...
        PSX_joystick.hwdata = NULL; // driver dependent info
        PSX_joystick.ref_count = 0; // no one's referenced yet...
    }
    
    return 0; // all good
}

SDL_Surface *SDL_SetVideoMode(int width, int height, int bpp, Uint32 flags)
{
    int i;
    SDL_Surface *screen_surface;
    
    if (width != 320 && width != 640) {
        printf("BJ: SDL_SetVideo Mode: Bad width: %d\n", width);
        return NULL;
    } else if (height != 240 && height != 480) {
        printf("BJ: SDL_SetVideo Mode: Bad height: %d\n", height);
        return NULL;
    }
    printf("BJ: SDL_SetVideoMode: %dx%d %d\n", width, height, bpp);

    /* Set screen size globals */
    S_WIDTH = width; S_HEIGHT = height;
    
    /* Set where to start uploading vram textures to */
    vram_tex_x = S_WIDTH;
    vram_tpage_x = vram_tex_x / 64;
    printf("Set vram tex x: %d\n", vram_tex_x);

    /* Basic init */
    PSX_Init();
    GsInit();
    GsSetList(prim_list);
    GsClearMem();

    /* Set Video Mode */
    //GsSetVideoMode(320, 240, VMODE_NTSC);
    GsSetVideoModeEx(S_WIDTH, S_HEIGHT, VMODE_NTSC,
        0,                          // rgb24
        (S_HEIGHT == 480) ? 1 : 0,  // interlacing? (required for height 480)
        0                           // reverse (should always be set to 0)
    );

    /* Load bios Font */
    //GsLoadFont(768, 0, 768, 256);
    GsLoadFont(960, 256, 960, 511);
    

    /* Function to be called each time image
     * is sent to tv */
    SetVBlankHandler(prog_vblank_handler);
    
    /* Now set the surface info... */
    //screen_surface = (SDL_Surface *)malloc(sizeof(SDL_Surface));
    screen_surface = &PSX_screensurface;
    memset(screen_surface, 0, sizeof(SDL_Surface));
    
    screen_surface->w = S_WIDTH;
    screen_surface->h = S_HEIGHT;
    //screen_surface->pixels = screen_buf;
    screen_surface->pixels = NULL;
    screen_surface->pitch = S_WIDTH * sizeof(unsigned short);
    screen_surface->clip_rect.x = 0; 
    screen_surface->clip_rect.y = 0;
    screen_surface->clip_rect.w = S_WIDTH;
    screen_surface->clip_rect.h = S_HEIGHT;
    
    /* Allocate the new format */
    //screen_surface->format = (SDL_PixelFormat *)malloc(sizeof(SDL_PixelFormat));
    screen_surface->format = &PSX_pixelformat;
    screen_surface->format->BitsPerPixel = 16;
    screen_surface->format->BytesPerPixel = 2;
    screen_surface->format->Rmask = 0x1F;
    screen_surface->format->Gmask = 0x1F;
    screen_surface->format->Bmask = 0x1F;
    screen_surface->format->Rshift = 0;
    screen_surface->format->Gshift = 5;
    screen_surface->format->Bshift = 10;

    /*
     * Keep track of the screen surface pointer
     */
    sdl_video_surface = screen_surface;
    
    return screen_surface;
}

void *SDL_revcpy(void *dst, const void *src, size_t len)
{
    char *srcp = (char *)src;
    char *dstp = (char *)dst;
    srcp += len-1;
    dstp += len-1;
    while ( len-- ) {
        *dstp-- = *srcp--;
    }
    return dst;
}

