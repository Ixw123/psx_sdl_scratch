#include "PSX_SDL.h"

SDL_BlitMap *SDL_AllocBlitMap(void)
{
	SDL_BlitMap *map;

	/* Allocate the empty map */
	map = (SDL_BlitMap *)SDL_malloc(sizeof(*map));
	if ( map == NULL ) {
		SDL_OutOfMemory();
		return(NULL);
	}
	SDL_memset(map, 0, sizeof(*map));

	/* Allocate the software blit data */
	map->sw_data = (struct private_swaccel *)SDL_malloc(sizeof(*map->sw_data));
	if ( map->sw_data == NULL ) {
		SDL_FreeBlitMap(map);
		SDL_OutOfMemory();
		return(NULL);
	}
	SDL_memset(map->sw_data, 0, sizeof(*map->sw_data));

	/* It's ready to go */
	return(map);
}

void SDL_FreeBlitMap(SDL_BlitMap *map)
{
	if ( map ) {
		SDL_InvalidateMap(map);
		if ( map->sw_data != NULL ) {
			SDL_free(map->sw_data);
		}
		SDL_free(map);
	}
}

void SDL_InvalidateMap(SDL_BlitMap *map)
{
	if ( ! map ) {
		return;
	}
	map->dst = NULL;
	map->format_version = (unsigned int)-1;
	if ( map->table ) {
		SDL_free(map->table);
		map->table = NULL;
	}
}

/* Figure out which of many blit routines to set up on a surface */
int SDL_CalculateBlit(SDL_Surface *surface)
{
	int blit_index;

	/* Clean everything out to start */
	/*if ( (surface->flags & SDL_RLEACCEL) == SDL_RLEACCEL ) {
		SDL_UnRLESurface(surface, 1);
	}*/
	surface->map->sw_blit = NULL;

	/* Figure out if an accelerated hardware blit is possible */
	surface->flags &= ~SDL_HWACCEL;
// bj commented out
#if 0
	if ( surface->map->identity ) {
		int hw_blit_ok;

		if ( (surface->flags & SDL_HWSURFACE) == SDL_HWSURFACE ) {
			/* We only support accelerated blitting to hardware */
			if ( surface->map->dst->flags & SDL_HWSURFACE ) {
				hw_blit_ok = current_video->info.blit_hw;
			} else {
				hw_blit_ok = 0;
			}
			if (hw_blit_ok && (surface->flags & SDL_SRCCOLORKEY)) {
				hw_blit_ok = current_video->info.blit_hw_CC;
			}
			if ( hw_blit_ok && (surface->flags & SDL_SRCALPHA) ) {
				hw_blit_ok = current_video->info.blit_hw_A;
			}
		} else {
			/* We only support accelerated blitting to hardware */
			if ( surface->map->dst->flags & SDL_HWSURFACE ) {
				hw_blit_ok = current_video->info.blit_sw;
			} else {
				hw_blit_ok = 0;
			}
			if (hw_blit_ok && (surface->flags & SDL_SRCCOLORKEY)) {
				hw_blit_ok = current_video->info.blit_sw_CC;
			}
			if ( hw_blit_ok && (surface->flags & SDL_SRCALPHA) ) {
				hw_blit_ok = current_video->info.blit_sw_A;
			}
		}
		if ( hw_blit_ok ) {
			SDL_VideoDevice *video = current_video;
			SDL_VideoDevice *this  = current_video;
			video->CheckHWBlit(this, surface, surface->map->dst);
		}
	}

	/* if an alpha pixel format is specified, we can accelerate alpha blits */
	if (((surface->flags & SDL_HWSURFACE) == SDL_HWSURFACE )&&(current_video->displayformatalphapixel))
	{
		if ( (surface->flags & SDL_SRCALPHA) )
			if ( current_video->info.blit_hw_A ) {
				SDL_VideoDevice *video = current_video;
				SDL_VideoDevice *this  = current_video;
				video->CheckHWBlit(this, surface, surface->map->dst);
			}
	}
#endif

	/* Get the blit function index, based on surface mode */
	/* { 0 = nothing, 1 = colorkey, 2 = alpha, 3 = colorkey+alpha } */
	blit_index = 0;
	blit_index |= (!!(surface->flags & SDL_SRCCOLORKEY))      << 0;
	if ( surface->flags & SDL_SRCALPHA
	     && (surface->format->alpha != SDL_ALPHA_OPAQUE
		 || surface->format->Amask) ) {
	        blit_index |= 2;
	}

	/* Check for special "identity" case -- copy blit */
	if ( surface->map->identity && blit_index == 0 ) {
	        surface->map->sw_data->blit = SDL_BlitCopy;

		/* Handle overlapping blits on the same surface */
		if ( surface == surface->map->dst ) {
		        surface->map->sw_data->blit = SDL_BlitCopyOverlap;
		}
	} else {
		if ( surface->format->BitsPerPixel < 8 ) {
			surface->map->sw_data->blit =
			    SDL_CalculateBlit0(surface, blit_index);
		} else {
			switch ( surface->format->BytesPerPixel ) {
			    case 1:
				surface->map->sw_data->blit =
				    SDL_CalculateBlit1(surface, blit_index);
				break;
			    case 2:
			    case 3:
			    case 4:
				surface->map->sw_data->blit =
				    SDL_CalculateBlitN(surface, blit_index);
				break;
			    default:
				surface->map->sw_data->blit = NULL;
				break;
			}
		}
	}
	/* Make sure we have a blit function */
	if ( surface->map->sw_data->blit == NULL ) {
		SDL_InvalidateMap(surface->map);
//		SDL_SetError("Blit combination not supported");
		SDL_DebugPrint("Blit combination not supported");
		return(-1);
	}

// bj commented out
#if 0
	/* Choose software blitting function */
	if(surface->flags & SDL_RLEACCELOK
	   && (surface->flags & SDL_HWACCEL) != SDL_HWACCEL) {

	        if(surface->map->identity
		   && (blit_index == 1
		       || (blit_index == 3 && !surface->format->Amask))) {
		        if ( SDL_RLESurface(surface) == 0 )
			        surface->map->sw_blit = SDL_RLEBlit;
		} else if(blit_index == 2 && surface->format->Amask) {
		        if ( SDL_RLESurface(surface) == 0 )
			        surface->map->sw_blit = SDL_RLEAlphaBlit;
		}
	}
#endif

	if ( surface->map->sw_blit == NULL ) {
		surface->map->sw_blit = SDL_SoftBlit;
	}
	return(0);
}

void SDL_BlitCopy(SDL_BlitInfo *info)
{
	Uint8 *src, *dst;
	int w, h;
	int srcskip, dstskip;

	w = info->d_width*info->dst->BytesPerPixel;
	h = info->d_height;
	src = info->s_pixels;
	dst = info->d_pixels;
	srcskip = w+info->s_skip;
	dstskip = w+info->d_skip;

#ifdef SSE_ASMBLIT
	if(SDL_HasSSE())
	{
		while ( h-- ) {
			SDL_memcpySSE(dst, src, w);
			src += srcskip;
			dst += dstskip;
		}
		__asm__ __volatile__ (
		"	emms\n"
		::);
	}
	else
#endif
#ifdef MMX_ASMBLIT
	if(SDL_HasMMX())
	{
		while ( h-- ) {
			SDL_memcpyMMX(dst, src, w);
			src += srcskip;
			dst += dstskip;
		}
		__asm__ __volatile__ (
		"	emms\n"
		::);
	}
	else
#endif
	while ( h-- ) {
		SDL_memcpy(dst, src, w);
		src += srcskip;
		dst += dstskip;
	}
}

void SDL_BlitCopyOverlap(SDL_BlitInfo *info)
{
	Uint8 *src, *dst;
	int w, h;
	int srcskip, dstskip;

	w = info->d_width*info->dst->BytesPerPixel;
	h = info->d_height;
	src = info->s_pixels;
	dst = info->d_pixels;
	srcskip = w+info->s_skip;
	dstskip = w+info->d_skip;
	if ( dst < src ) {
		while ( h-- ) {
			SDL_memmove(dst, src, w);
			src += srcskip;
			dst += dstskip;
		}
	} else {
		src += ((h-1) * srcskip);
		dst += ((h-1) * dstskip);
		while ( h-- ) {
			SDL_revcpy(dst, src, w);
			src -= srcskip;
			dst -= dstskip;
		}
	}
}

/* The general purpose software blit routine */
int SDL_SoftBlit(SDL_Surface *src, SDL_Rect *srcrect,
			SDL_Surface *dst, SDL_Rect *dstrect)
{
	int okay;
	int src_locked;
	int dst_locked;

	/* Everything is okay at the beginning...  */
	okay = 1;

	/* Lock the destination if it's in hardware */
	dst_locked = 0;
	if ( SDL_MUSTLOCK(dst) ) {
		if ( SDL_LockSurface(dst) < 0 ) {
			okay = 0;
		} else {
			dst_locked = 1;
		}
	}
	/* Lock the source if it's in hardware */
	src_locked = 0;
	if ( SDL_MUSTLOCK(src) ) {
		if ( SDL_LockSurface(src) < 0 ) {
			okay = 0;
		} else {
			src_locked = 1;
		}
	}

	/* Set up source and destination buffer pointers, and BLIT! */
	if ( okay  && srcrect->w && srcrect->h ) {
		SDL_BlitInfo info;
		SDL_loblit RunBlit;

		/* Set up the blit information */
		info.s_pixels = (Uint8 *)src->pixels +
				(Uint16)srcrect->y*src->pitch +
				(Uint16)srcrect->x*src->format->BytesPerPixel;
		info.s_width = srcrect->w;
		info.s_height = srcrect->h;
		info.s_skip=src->pitch-info.s_width*src->format->BytesPerPixel;
		info.d_pixels = (Uint8 *)dst->pixels +
				(Uint16)dstrect->y*dst->pitch +
				(Uint16)dstrect->x*dst->format->BytesPerPixel;
		info.d_width = dstrect->w;
		info.d_height = dstrect->h;
		info.d_skip=dst->pitch-info.d_width*dst->format->BytesPerPixel;
		info.aux_data = src->map->sw_data->aux_data;
		info.src = src->format;
		info.table = src->map->table;
		info.dst = dst->format;
		RunBlit = src->map->sw_data->blit;

		/* Run the actual software blit */
		RunBlit(&info);
	}

	/* We need to unlock the surfaces if they're locked */
	if ( dst_locked ) {
		SDL_UnlockSurface(dst);
	}
	if ( src_locked ) {
		SDL_UnlockSurface(src);
	}
	/* Blit is done! */
	return(okay ? 0 : -1);
}

int SDL_UpperBlit (SDL_Surface *src, SDL_Rect *srcrect,
		   SDL_Surface *dst, SDL_Rect *dstrect)
{
        SDL_Rect fulldst;
	int srcx, srcy, w, h;
    int i;

	/* Make sure the surfaces aren't locked */
	if ( ! src || ! dst ) {
		//SDL_SetError("SDL_UpperBlit: passed a NULL surface");
		SDL_DebugPrint("SDL_UpperBlit: passed a NULL surface");
		return(-1);
	}
	if ( src->locked || dst->locked ) {
		//SDL_SetError("Surfaces must not be locked during blit");
		SDL_DebugPrint("Surfaces must not be locked during blit");
		return(-1);
	}

	/* If the destination rectangle is NULL, use the entire dest surface */
	if ( dstrect == NULL ) {
	        fulldst.x = fulldst.y = 0;
		dstrect = &fulldst;
	}

    /* BJ added - if the width and height both equal the window size,
     * we'll assume its a background and make it its own separate sprite 
     * instead
     */
    //if (src->w == S_WIDTH && src->h == S_HEIGHT) {
    //    // TODO - upload THIS surface's pixels to the location of bg in vram
    //    LoadImage(src->pixels, 320, 0, S_WIDTH, S_HEIGHT);
    //    printf("BJ - loading BG sprite instead of copying\n");
    //}
    /* 
     * Bj added - check if the surface is in the list of loaded sprites
     */
    for (i=0; i<sdl_surface_count; ++i) {
        // TODO - this isn't working?!?!? says struct surface pointer is 0x0 (null)
        if (sdl_sprites[i].surface == src) {
            //printf("Found BJ mapped surface!\n");

            /* Set the X and Y of the sprite
             * make the X offset correctly */
            if (dstrect) {
                sdl_sprites[i].sprite.x = dstrect->x;
                sdl_sprites[i].sprite.y = dstrect->y;
            }

            /*
             * Set the u/v texpage clip correctly
             */
            if (srcrect) {
                sdl_sprites[i].sprite.u = srcrect->x;
                sdl_sprites[i].sprite.v = srcrect->y;
                sdl_sprites[i].sprite.w = srcrect->w;
                sdl_sprites[i].sprite.h = srcrect->h;
                
                //if (sdl_sprites[i].sprite.tpage == 12) {
                //    printf("Blitting grass: u,v: %d,%d\n", sdl_sprites[i].sprite.u, sdl_sprites[i].sprite.v);
                //}
            }

            /* Do the 'blitting', which in this case we are 
             * just telling the PSX we want to draw it in this order
             */
            GsSortSprite(&sdl_sprites[i].sprite);
            //printf("Sorting sprite %d\n", i);
#if 0 
            Decided this wouldn't work without a way to order the drawings

            // instead of doing the above, indicate we want to draw this sprite
            // and will be drawn in the loop run by SDL_Flip() instead
            // sdl_sprites[i].is_drawn = 1;
#endif

            /* Skip the rest of the function */
            return 0;
        }
        //else {
        //    printf("Umatched surfaces %d: 0x%X    0x%X\n", i, sdl_sprites[i].surface, src);
        //}
    }

	/* clip the source rectangle to the source surface */
	if(srcrect) {
	        int maxw, maxh;

		srcx = srcrect->x;
		w = srcrect->w;
		if(srcx < 0) {
		        w += srcx;
			dstrect->x -= srcx;
			srcx = 0;
		}
		maxw = src->w - srcx;
		if(maxw < w)
			w = maxw;

		srcy = srcrect->y;
		h = srcrect->h;
		if(srcy < 0) {
		        h += srcy;
			dstrect->y -= srcy;
			srcy = 0;
		}
		maxh = src->h - srcy;
		if(maxh < h)
			h = maxh;

	} else {
	        srcx = srcy = 0;
		w = src->w;
		h = src->h;
	}

	/* clip the destination rectangle against the clip rectangle */
	{
	        SDL_Rect *clip = &dst->clip_rect;
		int dx, dy;

		dx = clip->x - dstrect->x;
		if(dx > 0) {
			w -= dx;
			dstrect->x += dx;
			srcx += dx;
		}
		dx = dstrect->x + w - clip->x - clip->w;
		if(dx > 0)
			w -= dx;

		dy = clip->y - dstrect->y;
		if(dy > 0) {
			h -= dy;
			dstrect->y += dy;
			srcy += dy;
		}
		dy = dstrect->y + h - clip->y - clip->h;
		if(dy > 0)
			h -= dy;
	}

	if(w > 0 && h > 0) {
	        SDL_Rect sr;
	        sr.x = srcx;
		sr.y = srcy;
		sr.w = dstrect->w = w;
		sr.h = dstrect->h = h;
		return SDL_LowerBlit(src, &sr, dst, dstrect);
	}
	dstrect->w = dstrect->h = 0;
	return 0;
}

/*
 * Set up a blit between two surfaces -- split into three parts:
 * The upper part, SDL_UpperBlit(), performs clipping and rectangle
 * verification.  The lower part is a pointer to a low level
 * accelerated blitting function.
 *
 * These parts are separated out and each used internally by this
 * library in the optimimum places.  They are exported so that if
 * you know exactly what you are doing, you can optimize your code
 * by calling the one(s) you need.
 */
int SDL_LowerBlit (SDL_Surface *src, SDL_Rect *srcrect,
				SDL_Surface *dst, SDL_Rect *dstrect)
{
	SDL_blit do_blit;
	SDL_Rect hw_srcrect;
	SDL_Rect hw_dstrect;

	/* Check to make sure the blit mapping is valid */
	if ( (src->map->dst != dst) ||
             (src->map->dst->format_version != src->map->format_version) ) {
		if ( SDL_MapSurface(src, dst) < 0 ) {
			return(-1);
		}
	}

	/* Figure out which blitter to use */
	/*if ( (src->flags & SDL_HWACCEL) == SDL_HWACCEL ) {
		if ( src == SDL_VideoSurface ) {
			hw_srcrect = *srcrect;
			hw_srcrect.x += current_video->offset_x;
			hw_srcrect.y += current_video->offset_y;
			srcrect = &hw_srcrect;
		}
		if ( dst == SDL_VideoSurface ) {
			hw_dstrect = *dstrect;
			hw_dstrect.x += current_video->offset_x;
			hw_dstrect.y += current_video->offset_y;
			dstrect = &hw_dstrect;
		}
		do_blit = src->map->hw_blit;
	} else {*/
		do_blit = src->map->sw_blit;
	//}
	// BJ test
	if (do_blit == NULL || (!do_blit)) {
		printf("BJ Surface: blit is null!\n");
		return -1;
	}
	return(do_blit(src, srcrect, dst, dstrect));
}

