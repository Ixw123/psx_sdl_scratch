#include "soundtest.h"

/*
 * Variables from main 
 */
extern SDL_Event    event;
extern SDL_Surface *win_surface;
extern SDL_Joystick *joy1;

extern Mix_Music *bg_music;
extern Mix_Music *drummy_music;
extern Mix_Music *dark_music;
//Mix_Music *lazer_music = NULL;
extern Mix_Chunk *lazer_chunk;
extern Mix_Chunk *hit_chunk;
extern Mix_Chunk *victory_chunk;

struct message {
    char *text;
    int x;
    int y;
};

static struct message messages[6] = {
    {"CROSS      BG   MUSIC", 10, 10},
    {"CIRCLE     WIN  MUSIC", 10, 30},
    {"SQUARE     LOSE MUSIC", 10, 50},
    {"TRIANGLE   LAZER EFFECT", 10, 90},
    {"L1         HIT   EFFECT", 10, 110},
    {"R1         WIN   EFFECT", 10, 130}
};

// order same as listed above
static int mus_toggles[6] = {
    0,0,0,0,0,0
};

void sound_demo(void)
{
    int i;
    int done = 0;
    do
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) done = 1;
            
            else if (event.type == SDL_JOYBUTTONDOWN) {
                printf("Joybuttondown, main: %d\n", event.jbutton.button);
                
                switch (event.jbutton.button)
                {
                    // x
                    case 0:
                        printf("X main down event!\n");
                        if (!mus_toggles[0]) {
                            Mix_HaltMusic();
                            Mix_PlayMusic(bg_music, 1);
                        } else {
                            Mix_HaltMusic();
                        }
                        mus_toggles[0] = !mus_toggles[0];
                        //Mix_PlayMusic(lazer_music, 1);
                        //Mix_PlayChannel(-1, lazer_chunk, 0);
                        break;
                    // square
                    case 2:
                        printf("Square main down event!\n");
                        if (!mus_toggles[2]) {
                            Mix_HaltMusic();
                            Mix_PlayMusic(dark_music, 1);
                        } else {
                            Mix_HaltMusic();
                        }
                        mus_toggles[2] = !mus_toggles[2];
                        break;
                    // triangle
                    case 3:
                        printf("Triangle main down event!\n");
                        Mix_PlayChannel(-1, lazer_chunk, 0);
                        break;
                    // circle
                    case 1:
                        printf("Circle main down event!\n");
                        if (!mus_toggles[1]) {
                            Mix_HaltMusic();
                            Mix_PlayMusic(drummy_music, 1);
                        } else {
                            Mix_HaltMusic();
                        }
                        mus_toggles[1] = !mus_toggles[1];
                        break;
                    // L1
                    case 4:
                        printf("L1 main down event!\n");
                        Mix_PlayChannel(-1, hit_chunk, 0);
                        break;
                    // L2
                    case 10:
                        printf("L2 main down event!\n");
                        break;
                    // R1
                    case 5:
                        printf("R1 main down event!\n");
                        Mix_PlayChannel(-1, victory_chunk, 0);
                        break;
                    // R2
                    case 11:
                        printf("R2 main down event!\n");
                        break;
                    // Select
                    case 6:
                        printf("Select main down event!\n");
                        done = 1;
                        break;
                    // Start
                    case 7:
                        printf("Select start down event!\n");
                        done = 1;
                        break;
                    // L3
                    case 8:
                        printf("L3 down event!\n");
                        break;
                    // R3
                    case 9:
                        printf("R3 down event!\n");
                        break;
                    default:
                        printf("Unhandled button!\n");
                        break;
                }
            } // end if event type == JOYBUTTONDOWN
        }
        
        SDL_FillRect(win_surface, NULL, 0);
        
        /* Draw each label */
        for (i=0; i<6; i++)
        {
            draw_text(win_surface, messages[i].text, messages[i].x, messages[i].y);
        }
        
        SDL_Flip(win_surface);
        
        SDL_Delay(1000/60);
        
    } while(!done);
    
    Mix_HaltMusic();
}
