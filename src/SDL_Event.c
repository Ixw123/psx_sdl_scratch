#include "PSX_SDL.h"

/* Buffer for the controller */
static unsigned short padbuf = 0;
static unsigned short prev_padbuf = 0; // keep track of the last read for SDL events

static char leftanalog[2] = { 0, 0 }; // x and y
static char leftanalog_prev[2] = { 0, 0 }; // previous x and y

static char rightanalog[2] = { 0, 0 }; // x and y
static char rightanalog_prev[2] = { 0, 0 }; // previous x and y

static psx_pad_state pad_state; // holds more advanced pad info (ie joystick)

SDL_Joystick *SDL_JoystickOpen(int index)
{
    return &PSX_joystick;
}

int SDL_PollEvent (SDL_Event *event)
{
    const int ANALOG_THRESH = 30; // how much the analog has to be
                                  // moved to register as an event
    
    /* Update the pad buffers */
    //psx_pad_state pad_state;    // struct to hold pad information
    //prev_padbuf = padbuf;       // copy the current into the old
    //PSX_ReadPad(&padbuf, NULL); // the second arg would be a second controller?
    PSX_PollPad(0, &pad_state);     // more advanced pad reading (to get joystick info)
    prev_padbuf = padbuf;
    padbuf = pad_state.buttons;
    
    // values are from -128 to 127
    //char joyX = pad_state.extra.analogJoy.x[0]; // 0 is right joystick, 1 is left joystick
    //char joyY = pad_state.extra.analogJoy.y[0];
    
    leftanalog_prev[0] = leftanalog[0];
    leftanalog_prev[1] = leftanalog[1];
    
    leftanalog[0] = pad_state.extra.analogJoy.x[1];
    leftanalog[1] = pad_state.extra.analogJoy.y[1];
    
    rightanalog_prev[0] = rightanalog[0];
    rightanalog_prev[1] = rightanalog[1];
    
    rightanalog[0] = pad_state.extra.analogJoy.x[0];
    rightanalog[1] = pad_state.extra.analogJoy.y[0];
    
/*
 * Analog Sticks
 */
    // Left analog X
    if ((leftanalog[0] > ANALOG_THRESH || 
         leftanalog[0] < -ANALOG_THRESH) &&
         leftanalog[0] != leftanalog_prev[0]) {
        
        printf("left Analog X Event\n");
        
        event->jaxis.type = SDL_JOYAXISMOTION;
        event->jaxis.axis = 0; // 0 = x axis left
        event->jaxis.which = 0; // always 0 for first controller
        event->jaxis.value = leftanalog[0] * 255; // map from -127..128 to -32768..32767
        
        return 1;
    } 
    
    // Left analog Y
    else if ((leftanalog[1] > ANALOG_THRESH || 
         leftanalog[1] < -ANALOG_THRESH) &&
         leftanalog[1] != leftanalog_prev[1]) {
        
        printf("Left Analog Y Event\n");
        
        event->jaxis.type = SDL_JOYAXISMOTION;
        event->jaxis.axis = 1; // 1 = Y axis left
        event->jaxis.which = 0; // always 0 for first controller
        event->jaxis.value = leftanalog[1] * 255; // map from -127..128 to -32768..32767
        
        return 1;
    }
    
    // Right analog X
    if ((rightanalog[0] > ANALOG_THRESH || 
         rightanalog[0] < -ANALOG_THRESH) &&
         rightanalog[0] != rightanalog_prev[0]) {
        
        printf("Right Analog X Event\n");
        
        event->jaxis.type = SDL_JOYAXISMOTION;
        event->jaxis.axis = 4; // 4 = x axis right
        event->jaxis.which = 0; // always 0 for first controller
        event->jaxis.value = rightanalog[0] * 255; // map from -127..128 to -32768..32767
        
        return 1;
    } 
    
    // Right analog Y
    else if ((rightanalog[1] > ANALOG_THRESH || 
         rightanalog[1] < -ANALOG_THRESH) &&
         rightanalog[1] != rightanalog_prev[1]) {
        
        printf("Right Analog Y Event\n");
        
        event->jaxis.type = SDL_JOYAXISMOTION;
        event->jaxis.axis = 3; // 3 = Y axis right
        event->jaxis.which = 0; // always 0 for first controller
        event->jaxis.value = rightanalog[1] * 255; // map from -127..128 to -32768..32767
        
        return 1;
    }
    
    if (padbuf == prev_padbuf) {
        //printf("New and old PSX padbuf are the same, returning!\n");
        return 0;
    }
    
    // intiailize value for now, since the buttons OR it...
    event->type = 0;
    
/*
 * Hat / Directional buttons
 */ 
    
    // UpLeft
    if ((padbuf & PAD_LEFT) && (padbuf & PAD_UP)) {
        printf("Upleft Event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0;
        event->jhat.value = SDL_HAT_LEFTUP;
        event->jhat.which = 0;
    }
    // DownLeft
    else if ((padbuf & PAD_LEFT) && (padbuf & PAD_DOWN)) {
        printf("Downleft Event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0;
        event->jhat.value = SDL_HAT_LEFTDOWN;
        event->jhat.which = 0;
    }
    // UpRight
    else if ((padbuf & PAD_RIGHT) && (padbuf & PAD_UP)) {
        printf("UpRight Event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0;
        event->jhat.value = SDL_HAT_RIGHTUP;
        event->jhat.which = 0;
    }
    // DownRight
    else if ((padbuf & PAD_RIGHT) && (padbuf & PAD_DOWN)) {
        printf("Downright Event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0;
        event->jhat.value = SDL_HAT_RIGHTDOWN;
        event->jhat.which = 0;
    }
    // Left
    else if (padbuf & PAD_LEFT) {
        printf("Left event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0; // the first hat on the pad
        event->jhat.value = SDL_HAT_LEFT;
        event->jhat.which = 0; // pad 0 (for now...)
    }
    // Right
    else if (padbuf & PAD_RIGHT) {
        printf("Right event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0; // the first hat on the pad
        event->jhat.value = SDL_HAT_RIGHT;
        event->jhat.which = 0;
    }
    // Up
    else if (padbuf & PAD_UP) {
        printf("Up Event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0; // the first hat on the pad
        event->jhat.value = SDL_HAT_UP;
        event->jhat.which = 0;
    }
    // Down
    else if (padbuf & PAD_DOWN) {
        printf("Down event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0; // the first hat on the pad
        event->jhat.value = SDL_HAT_DOWN;
        event->jhat.which = 0;
    }
    // up, down, left, right take up bits 12 - 15
    else if (padbuf >> 12 == 0) {
        printf("Centered event!\n");
        event->type = SDL_JOYHATMOTION;
        event->jhat.hat = 0;
        event->jhat.value = SDL_HAT_CENTERED;
        event->jhat.which = 0;
    }
    
/*
 * Main buttons (square/triangle/circle/cross)
 */
    // cross
    if ((padbuf & PAD_CROSS) && !(prev_padbuf & PAD_CROSS)) {
        printf("Cross Pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 0; // X == 0
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_CROSS) && (prev_padbuf & PAD_CROSS)) {
        printf("Cross Released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 0; // X == 0
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // square
    if ((padbuf & PAD_SQUARE) && !(prev_padbuf & PAD_SQUARE)) {
        printf("Square pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 2; // square == 2
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_SQUARE) && (prev_padbuf & PAD_SQUARE)) {
        printf("Square released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 2; // square == 2
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // triangle
    if ((padbuf & PAD_TRIANGLE) && !(prev_padbuf & PAD_TRIANGLE)) {
        printf("Triangle pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 3; // triangle == 3
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_TRIANGLE) && (prev_padbuf & PAD_TRIANGLE)) {
        printf("Triangle released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 3; // triangle == 3
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // circle
    if ((padbuf & PAD_CIRCLE) && !(prev_padbuf & PAD_CIRCLE)) {
        printf("Circle pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 1; // circle == 1
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_CIRCLE) && (prev_padbuf & PAD_CIRCLE)) {
        printf("Circle released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 1; // square == 1
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
/*
 * Other buttons (L1/R1,select,start,etc.)
 */
    // L1
    if ((padbuf & PAD_L1) && !(prev_padbuf & PAD_L1)) {
        printf("L1 Pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 4; // L1 == 4
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_L1) && (prev_padbuf & PAD_L1)) {
        printf("L1 Released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 4; // L1 == 4
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // L2
    if ((padbuf & PAD_L2) && !(prev_padbuf & PAD_L2)) {
        printf("L2 pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 10; // L2 == 10
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_L2) && (prev_padbuf & PAD_L2)) {
        printf("L2 released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 10; // L2 == 10
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // R1
    if ((padbuf & PAD_R1) && !(prev_padbuf & PAD_R1)) {
        printf("R1 pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 5; // R1 == 5
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_R1) && (prev_padbuf & PAD_R1)) {
        printf("R1 released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 5; // R1 == 5
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // R2
    if ((padbuf & PAD_R2) && !(prev_padbuf & PAD_R2)) {
        printf("R2 pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 11; // R2 == 11
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_R2) && (prev_padbuf & PAD_R2)) {
        printf("R2 released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 11; // R2 == 11
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // Select
    if ((padbuf & PAD_SELECT) && !(prev_padbuf & PAD_SELECT)) {
        printf("Select pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 6; // Select == 6
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_SELECT) && (prev_padbuf & PAD_SELECT)) {
        printf("Select released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 6; // Select == 6
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // Start
    if ((padbuf & PAD_START) && !(prev_padbuf & PAD_START)) {
        printf("Start pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 7; // Start == 7
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_START) && (prev_padbuf & PAD_START)) {
        printf("Start released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 7; // Start == 7
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    
    // L3
    if ((padbuf & PAD_LANALOGB) && !(prev_padbuf & PAD_LANALOGB)) {
        printf("L3 pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 8; // L3 == 8
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_LANALOGB) && (prev_padbuf & PAD_LANALOGB)) {
        printf("L3 released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 8; // L3 == 8
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    // R3
    if ((padbuf & PAD_RANALOGB) && !(prev_padbuf & PAD_RANALOGB)) {
        printf("R3 pressed event!\n");
        event->type |= SDL_JOYBUTTONDOWN;
        event->jbutton.type = SDL_JOYBUTTONDOWN;
        event->jbutton.button = 9; // R3 == 9
        event->jbutton.state = SDL_PRESSED;
        event->jbutton.which = 0; // joystick index 0
    }
    else if (!(padbuf & PAD_RANALOGB) && (prev_padbuf & PAD_RANALOGB)) {
        printf("R3 released event!\n");
        event->type |= SDL_JOYBUTTONUP;
        event->jbutton.type = SDL_JOYBUTTONUP;
        event->jbutton.button = 9; // R3 == 9
        event->jbutton.state = SDL_RELEASED;
        event->jbutton.which = 0; // joystick index 0
    }
    
    return 1;
}

void SDL_JoystickUpdate(void)
{
    PSX_PollPad(0, &pad_state);     // more advanced pad reading (to get joystick info)
}

Sint16 SDL_JoystickGetAxis(SDL_Joystick *joystick, int axis)
{
    Sint16 val = 0;

    // TODO - when adding multiple controllers, will need to 
    // check the joystick pointer

    // 0 = x axis left
    // 1 = Y axis left
    // 4 = x axis right
    // 3 = Y axis right

    //PSX_PollPad(0, &pad_state);     // more advanced pad reading (to get joystick info)


    switch (axis)
    {
        case 0:
            val = pad_state.extra.analogJoy.x[1]; // 1 is left
            break;
        case 1:
            val = pad_state.extra.analogJoy.y[1]; // 1 is left
            break;
        case 3:
            val = pad_state.extra.analogJoy.y[0]; // 0 is right
            break;
        case 4:
            val = pad_state.extra.analogJoy.x[0]; // 0 is right
            break;
        default:
            SDL_DebugPrint("Unhandled joystick get axis");
            break;
    }

    // map from -128..128 to -32768..32768
    val *= 255;

    return val;
}

Uint8 SDL_JoystickGetHat(SDL_Joystick *joystick, int hat)
{
    Uint8 val;
    
    //static psx_pad_state local_pad_state;
    //PSX_PollPad(0, &local_pad_state);     // more advanced pad reading (to get joystick info))
    //padbuf = local_pad_state.buttons;

    if (hat != 0) {
        SDL_DebugPrint("Unhandled joystick hat");
        return SDL_HAT_CENTERED;
    }

    if      ((padbuf & PAD_UP) && (padbuf & PAD_LEFT))  val = SDL_HAT_LEFTUP;
    else if ((padbuf & PAD_UP) && (padbuf & PAD_RIGHT)) val = SDL_HAT_RIGHTUP;
    else if ((padbuf & PAD_DOWN) && (padbuf & PAD_LEFT)) val = SDL_HAT_LEFTDOWN;
    else if ((padbuf & PAD_DOWN) && (padbuf & PAD_RIGHT)) val = SDL_HAT_RIGHTDOWN;
    else if (padbuf & PAD_UP)   val = SDL_HAT_UP;
    else if (padbuf & PAD_DOWN) val = SDL_HAT_DOWN;
    else if (padbuf & PAD_LEFT) val = SDL_HAT_LEFT;
    else if (padbuf & PAD_RIGHT) val = SDL_HAT_RIGHT;

    return val;
}
