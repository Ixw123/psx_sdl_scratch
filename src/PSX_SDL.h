/*
 * SDL.h
 */

#ifndef _BJ_SDL_H_
#define _BJ_SDL_H_

#include <stdio.h>
#include <stdlib.h>
#include <psx.h>

#include "SDL.h"
#include "SDL_video.h"
#include "SDL_rwops.h"
#include "SDL_blit.h"

//#define S_WIDTH  255
//#define S_HEIGHT 240
extern int S_WIDTH;
extern int S_HEIGHT;

int          SDL_Init(Uint32 flags);
SDL_Surface *SDL_SetVideoMode(int width, int height, int bpp, Uint32 flags);
int          SDL_Flip(SDL_Surface *screen);

/*
 * A function to calculate the intersection of two rectangles:
 * return true if the rectangles intersect, false otherwise
 */
static __inline__
SDL_bool SDL_IntersectRect(const SDL_Rect *A, const SDL_Rect *B, SDL_Rect *intersection);
/*
 * Set the clipping rectangle for a blittable surface
 */
SDL_bool SDL_SetClipRect(SDL_Surface *surface, const SDL_Rect *rect);
int      SDL_FillRect(SDL_Surface *dst, SDL_Rect *dstrect, Uint32 color);

/*
 * File IO
 */
SDL_RWops *SDL_AllocRW(void);
SDL_RWops *SDL_RWFromFP(FILE *fp, int autoclose);
SDL_RWops *SDL_RWFromFile(const char *file, const char *mode);
void       SDL_FreeRW(SDL_RWops *area);

static int stdio_seek(SDL_RWops *context, int offset, int whence);
static int stdio_read(SDL_RWops *context, void *ptr, int size, int maxnum);
static int stdio_write(SDL_RWops *context, const void *ptr, int size, int num);
static int stdio_close(SDL_RWops *context);

/* From SDL_string.c in stdlib */
void *SDL_revcpy(void *dst, const void *src, size_t len);

Uint16 SDL_ReadLE16 (SDL_RWops *src);
Uint16 SDL_ReadBE16 (SDL_RWops *src);
Uint32 SDL_ReadLE32 (SDL_RWops *src);
Uint32 SDL_ReadBE32 (SDL_RWops *src);

/* Load BMP */
SDL_Surface *SDL_LoadBMP_RW(SDL_RWops *src, int freesrc);

/** Convenience macro -- load a surface from a file */
#define SDL_LoadBMP(file)	SDL_LoadBMP_RW(SDL_RWFromFile(file, "rb"), 1)

/* Bj testing */
void SDL_DebugPrint(char *message);
void SDL_Error(SDL_errorcode code);

/* For SDL_MapRGB */
Uint8 SDL_FindColor(SDL_Palette *pal, Uint8 r, Uint8 g, Uint8 b);
Uint32 SDL_MapRGB
(const SDL_PixelFormat * const format,
 const Uint8 r, const Uint8 g, const Uint8 b);
Uint16 SDL_CalculatePitch(SDL_Surface *surface);

SDL_BlitMap *SDL_AllocBlitMap(void);
void SDL_FreeBlitMap(SDL_BlitMap *map);
void SDL_InvalidateMap(SDL_BlitMap *map);

/* Map from Palette to Palette */
static Uint8 *Map1to1(SDL_Palette *src, SDL_Palette *dst, int *identical);
/* Map from Palette to BitField */
static Uint8 *Map1toN(SDL_PixelFormat *src, SDL_PixelFormat *dst);
/* Map from BitField to Dithered-Palette to Palette */
static Uint8 *MapNto1(SDL_PixelFormat *src, SDL_PixelFormat *dst, int *identical);
int SDL_MapSurface (SDL_Surface *src, SDL_Surface *dst);

/*
 * Calculate an 8-bit (3 red, 3 green, 2 blue) dithered palette of colors
 */
void SDL_DitherColors(SDL_Color *colors, int bpp);

/* Helper functions for SDL_BlitSurface */
void SDL_BlitCopy(SDL_BlitInfo *info);
void SDL_BlitCopyOverlap(SDL_BlitInfo *info);
/* The general purpose software blit routine */
int SDL_SoftBlit(SDL_Surface *src, SDL_Rect *srcrect,
			SDL_Surface *dst, SDL_Rect *dstrect);

int SDL_UpperBlit (SDL_Surface *src, SDL_Rect *srcrect,
		   SDL_Surface *dst, SDL_Rect *dstrect);
int SDL_LowerBlit (SDL_Surface *src, SDL_Rect *srcrect,
				SDL_Surface *dst, SDL_Rect *dstrect);

/*
 * Change any previous mappings from/to the new surface format
 */
void SDL_FormatChanged(SDL_Surface *surface);

/*
 * Free a previously allocated format structure
 */
void SDL_FreeFormat(SDL_PixelFormat *format);

/*
 * Create an empty RGB surface of the appropriate depth
 */
SDL_PixelFormat *SDL_AllocFormat(int bpp,
			Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
SDL_Surface * SDL_CreateRGBSurface (Uint32 flags,
			int width, int height, int depth,
			Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
/*
 * Free a surface created by the above function.
 */
void SDL_FreeSurface (SDL_Surface *surface);

/*
 * Unlock a previously locked surface
 */
void SDL_UnlockSurface (SDL_Surface *surface);
/*
 * Lock a surface to directly access the pixels
 */
int SDL_LockSurface (SDL_Surface *surface);


/* Pad functions */
SDL_Joystick *SDL_JoystickOpen(int index);
void          SDL_JoystickUpdate(void);
Sint16        SDL_JoystickGetAxis(SDL_Joystick *joystick, int axis);
Uint8         SDL_JoystickGetHat(SDL_Joystick *joystick, int hat);

/* Event functions */
int SDL_PollEvent (SDL_Event *event);

#ifndef SDL_memset4
#define SDL_memset4(dst, val, len)		\
do {						\
	unsigned _count = (len);		\
	unsigned _n = (_count + 3) / 4;		\
	Uint32 *_p = SDL_static_cast(Uint32 *, dst);	\
	Uint32 _val = (val);			\
	if (len == 0) break;			\
        switch (_count % 4) {			\
        case 0: do {    *_p++ = _val;		\
        case 3:         *_p++ = _val;		\
        case 2:         *_p++ = _val;		\
        case 1:         *_p++ = _val;		\
		} while ( --_n );		\
	}					\
} while(0)
#endif

// bj added to make this always false
// also added to SDL_blit_N.c
#define SDL_HasMMX() \
	(0)

// bj added to pair an SDL surface with a psx-sdk sprite,
// to map a surface with a psx vram sprite texture
typedef struct {
    GsSprite    sprite;
    SDL_Surface *surface;
    //uint8_t     is_drawn; // 1 or 0 depending if the user called SDL_BlitSurface(), indicating we want to draw it this frame or not
} SpriteKey;

// dummy to make it do nothing; psx runs at 60Hz I believe
#define SDL_Delay(ms) \
	;
	//printf("SDL Delay called\n");

// PSX automatically uses black (0,0,0) as transparent
#define SDL_SetColorKey(surface, flag, key) \
	;
	//printf("\n\n\n\n    SDL_SetColorKey called\n\n\n\n")

/*
 * Keep track of all of the surfaces allocated
 */
extern uint32_t sdl_surface_count;
#define MAX_SPRITES 10
extern SpriteKey sdl_sprites[MAX_SPRITES];

/*
 * Keep track of the surface returned by SDL_SetVideoMode so we know when the user
 * is trying to draw directly to the BG
 */
extern SDL_Surface *sdl_video_surface;

/*
 * Keep track of where stuff's been uploaded to vram
 */
extern uint32_t vram_tex_x;
extern uint32_t vram_tex_y;
extern uint8_t vram_tpage_x; // relative texture page (e.g. 7,0)
extern uint8_t vram_tpage_y;
extern uint32_t vram_clut_x;
extern uint32_t vram_clut_y;

/*
 * PSX Video Variables
 */
extern int dbuf;
extern volatile int display_is_old;


extern SDL_Joystick PSX_joystick;

#endif // _BJ_SDL_H_
