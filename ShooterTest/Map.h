/*
 * Map.h
 */
#ifndef _MAP_H_INCLUDED_
#define _MAP_H_INCLUDED_

#include "SDL.h"
#include "Sprite.h"

typedef struct {
    int *data; // tile info
    Sprite *sprites; // different tiles
    int width; // number of tiles width and height
    int height;
} Map;

// arguments - screen width and screen height
void init_map(Map *map, int width, int height);
void draw_map(Map *map, SDL_Surface *screen);

#endif

