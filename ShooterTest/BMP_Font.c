/*
 * BMP_Font.c
 */

#include "BMP_Font.h"

static SDL_Surface *font_surface = NULL;
static int char_width;
static int char_height;
static SDL_Rect message_clip_rect; // sprite clipping
static SDL_Rect message_loc_rect; // screen location
static int char_spacing; // how many pixels of space between each character

/* Map from a 1d array to a 2d array for character locations */
typedef struct {
    int x;
    int y;
} char_loc;
static char_loc character_map[16*14];
static char_loc number_map[7*2];

// this was for the previous bitmap font used
#if 0
int load_font(const char *filename)
{
    int x,y;

    /* Load the bitmap font image */
    if (!(font_surface = SDL_LoadBMP(filename))) {
        printf("Error loading font: %s\n", filename);
        return -1;
    }

    /* Make black transparent */
    SDL_SetColorKey(font_surface, SDL_SRCCOLORKEY, SDL_MapRGB(font_surface->format, 0,0,0));

    /* This isn't particuarly general purpose or anything
     * It's specific to the bitmap image file i'm loading 
     */
    char_width = font_surface->w / 16;
    char_height = font_surface->h / 14;

    /* Just arbitrarily chose 5, but it seems alright */
    char_spacing = char_width - 5;

    message_clip_rect = (SDL_Rect){0,0, char_width, char_height};
    message_loc_rect = (SDL_Rect){0,0, char_width, char_height};

    /* Create the character location map */
    for (x=0; x<16; ++x)
        for (y=0; y<14; ++y) {
            character_map[y*16 + x].x = x*char_width;
            character_map[y*16 + x].y = y*char_height;
        }

    return 0;
}
#endif

int load_font(const char *filename)
{
    int x,y;

    /* Load the bitmap font image */
    if (!(font_surface = SDL_LoadBMP(filename))) {
        printf("Error loading font: %s\n", filename);
        return -1;
    }

    /* Make black transparent */
    SDL_SetColorKey(font_surface, SDL_SRCCOLORKEY, SDL_MapRGB(font_surface->format, 0,0,0));
    
    /* This isn't particuarly general purpose or anything
     * It's specific to the bitmap image file i'm loading 
     */
    char_width = font_surface->w / 7;
    char_height = font_surface->h / 6;
    
    char_spacing = char_width;
    
    message_clip_rect = (SDL_Rect){0,0, char_width, char_height};
    message_loc_rect = (SDL_Rect){0,0, char_width, char_height};
    
    /* Create character location maps */
    for (x=0; x<7; ++x) {
        /* Letters */
        for (y=0; y<4; ++y) {
            character_map[y*7+x].x = x*char_width;
            character_map[y*7+x].y = y*char_height;
        }
        
        /* Numbers */
        for (y=0; y<2; ++y) {
            number_map[y*7+x].x = x*char_width;
            number_map[y*7+x].y = (y+4)*char_height;
        }
    }
    
    return 0;
}

// this was for the old bitmap font
#if 0
int draw_text(SDL_Surface *screen, const char *message, int x, int y)
{
    int i;
    int message_len = strlen(message);
    int char_index; // the location of the character in the bitmap if it was a 1d array

    for (i = 0; i < message_len; ++i) 
    {
        /* ASCII codes, 32 = space = first character we have */
        char_index = message[i] - 32;

        /* Get the character offset */
        message_clip_rect.x = character_map[char_index].x;
        message_clip_rect.y = character_map[char_index].y;

        /* Set its location on the screen */
        message_loc_rect.x = x;
        message_loc_rect.y = y;

        SDL_BlitSurface(font_surface, &message_clip_rect, screen, &message_loc_rect);

        /* Increment its location */
        x += char_spacing;
    }

    return 0;
}
#endif

int draw_text(SDL_Surface *screen, const char *message, int x, int y)
{
    int i;
    int message_len = strlen(message);
    int char_index; // the location of the character in the bitmap if it was a 1d array

    for (i = 0; i < message_len; ++i) 
    {
        if (message[i] == ' ') {
            message_clip_rect.x = 5*char_width;
            message_clip_rect.y = 3*char_height;
        }
        
        /* Check if the ASCII char is a number or letter */
        else if (message[i] < 65) {
            /* ASCII codes, 48 = 0 = first number character we have */
            char_index = message[i] - 48;
            message_clip_rect.x = number_map[char_index].x;
            message_clip_rect.y = number_map[char_index].y;
        }
        
        else {
            /* ASCII codes, 65 = A = first letter character we have */
            char_index = message[i] - 65;
            message_clip_rect.x = character_map[char_index].x;
            message_clip_rect.y = character_map[char_index].y;
        }
        
        /* Set its location on the screen */
        message_loc_rect.x = x;
        message_loc_rect.y = y;

        SDL_BlitSurface(font_surface, &message_clip_rect, screen, &message_loc_rect);

        /* Increment its location */
        x += char_spacing;
    }

    return 0;
}
