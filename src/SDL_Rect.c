#include "PSX_SDL.h"

/*
 * A function to calculate the intersection of two rectangles:
 * return true if the rectangles intersect, false otherwise
 */
static __inline__
SDL_bool SDL_IntersectRect(const SDL_Rect *A, const SDL_Rect *B, SDL_Rect *intersection)
{
	int Amin, Amax, Bmin, Bmax;

	/* Horizontal intersection */
	Amin = A->x;
	Amax = Amin + A->w;
	Bmin = B->x;
	Bmax = Bmin + B->w;
	if(Bmin > Amin)
	        Amin = Bmin;
	intersection->x = Amin;
	if(Bmax < Amax)
	        Amax = Bmax;
	intersection->w = Amax - Amin > 0 ? Amax - Amin : 0;

	/* Vertical intersection */
	Amin = A->y;
	Amax = Amin + A->h;
	Bmin = B->y;
	Bmax = Bmin + B->h;
	if(Bmin > Amin)
	        Amin = Bmin;
	intersection->y = Amin;
	if(Bmax < Amax)
	        Amax = Bmax;
	intersection->h = Amax - Amin > 0 ? Amax - Amin : 0;

	return (intersection->w && intersection->h);
}

/*
 * Set the clipping rectangle for a blittable surface
 */
SDL_bool SDL_SetClipRect(SDL_Surface *surface, const SDL_Rect *rect)
{
	SDL_Rect full_rect;

	/* Don't do anything if there's no surface to act on */
	if ( ! surface ) {
		return SDL_FALSE;
	}

	/* Set up the full surface rectangle */
	full_rect.x = 0;
	full_rect.y = 0;
	full_rect.w = surface->w;
	full_rect.h = surface->h;

	/* Set the clipping rectangle */
	if ( ! rect ) {
		surface->clip_rect = full_rect;
		return 1;
	}
	return SDL_IntersectRect(rect, &full_rect, &surface->clip_rect);
}



int SDL_FillRect(SDL_Surface *dst, SDL_Rect *dstrect, Uint32 color)
{
    //printf("IN SDL FILLRECT!\n");
    
    /* Pointer to the current row to fill in */
    Uint8 *row;
    int x, y;
    //int color_scale = 255 / 31; // the color will be from 0..31, we can map from 0..255
    GsRectangle psx_rect; // we can directly draw a primitive if we're drawing to the background

    /* If we're filling a rectangle directly to the background... */
    if (dst == sdl_video_surface) {

        /* If no rect was given, we want to fill the entire background */
        if (!dstrect) {
            // extract the RGB components
            GsSortCls((color&31)<<3,((color>>5)&31)<<3,((color>>10)&31)<<3);
        }
        else {
            psx_rect.x = dstrect->x; psx_rect.y = dstrect->y;
            psx_rect.w = dstrect->w; psx_rect.h = dstrect->h;

            psx_rect.r = (color         & 31)<<3;
            psx_rect.g = ((color >> 5)  & 31)<<3;
            psx_rect.b = ((color >> 10) & 31)<<3;
            psx_rect.attribute = 0;

            GsSortRectangle(&psx_rect);
        }

        /* Exit the function, we're done now */
        return 0;
    }

    
    /* If dstrect is null, then we just fill in the entire surface */
    if (!dstrect) {
        //printf("In NOT dstrect!\n");
        dstrect = &dst->clip_rect;
    }
    
    row = (Uint8 *)dst->pixels+dstrect->y*dst->pitch+
            dstrect->x*dst->format->BytesPerPixel;
    
    //printf("Get row values: dstrect y, dst pitch, dstrect x, dst bytesperpixel: %d, %d, %d, %d\n", dstrect->y, dst->pitch, dstrect->x, dst->format->BytesPerPixel);
        
    switch(dst->format->BytesPerPixel) {
        case 2:
            //printf("In case 2!\n");
            for (y = dstrect->h; y; --y) {
                //printf("In y loop, y: %d\n", y);
                Uint16 *pixels = (Uint16 *)row;
                Uint16 c = (Uint16)color;
                Uint32 cc = (Uint32)c << 16 | c;
                int n = dstrect->w;
                if ((uintptr_t)pixels & 3) {
                    *pixels++ = c;
                    n--;
                }
                if (n >> 1) {
                    SDL_memset4(pixels, cc, n >> 1);
                }
                if (n & 1) {
                    pixels[n - 1] = c;
                }
                row += dst->pitch;
            }
            break;
        default:
            printf("Fill Rect: Unsupported Bytes Per Pixel: %d\n", dst->format->BytesPerPixel);
            return -1;
            break;
    }
    
    return 0;
}

