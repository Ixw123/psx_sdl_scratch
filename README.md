# PSX SDL 1.2 Port

##Compiling

You'll need PSXSDK installed in order to compile; I assume you'll be compiling from a unix/linux system with
PSXSDK installed in /usr/local/psxsdk

Other than that a simple 'make' should build the library.

##Example

An example application is in the directory 'ShooterTest'. You compile the PSX version with `make -f Makefile.psx`.
The file `Makefile` is for building on Windows with a mingw setup. On linux the application can be compiled
with `gcc *.c -o main_linux -lSDL -lSDL_mixer -I/usr/include/SDL`.

##Available functions

This is only a partial implementation; the functions available to you can be found in `src/PSX_SDL.h` and 
`mix_src/PSX_mixer.h`