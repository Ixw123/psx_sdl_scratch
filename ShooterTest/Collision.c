/*
 * Collision.c
 */

#include "Collision.h"

int isCollision(SDL_Rect *rect1, SDL_Rect *rect2)
{
    int res = 0;

    if ((rect1->x + rect1->w > rect2->x) && 
        (rect1->x < rect2->x + rect2->w) && 
        (rect1->y+rect1->h > rect2->y) && 
        (rect1->y < rect2->y + rect2->h)) res = 1;

    return res;
}

