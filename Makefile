#include ../../Makefile.cfg

TOOLCHAIN_PREFIX = /usr/local/psxsdk

CC = psx-gcc
AR = mipsel-unknown-elf-ar

CDLIC_FILE = $(TOOLCHAIN_PREFIX)/share/licenses/infousa.dat

TARGET = main

#SOURCES = main.c Sprite.c

OBJS = src/SDL.o src/SDL_rwops.o src/SDL_BMP.o src/SDL_surface.o src/SDL_blit_0.o \
	   src/SDL_blit_1.o src/SDL_blit_N.o src/SDL_blit_A.o src/SDL_Blit.o \
	   src/SDL_Error.o src/SDL_Colors.o src/SDL_Event.o src/SDL_Rect.o

MIX_OBJS = mix_src/PSX_mixer.o

LIB = lib/libSDL.a
MIX_LIB = lib/libSDL_mixer.a

CFLAGS = -D __BJ_PSX__ -g -std=c99

INCDIRS = -I./include -I./src -I./SDL_mixer-1.2.12

#all:
#	mkdir -p cd_root
#	$(CC) $(CFLAGS) -Iinclude -DEXAMPLES_VMODE=VMODE_NTSC $(SOURCES) -o $(TARGET).elf
#	elf2exe $(TARGET).elf $(TARGET).exe
#	cp $(TARGET).exe cd_root
#	bmp2tim image.bmp cd_root/image.tim 16 -org=320,0
#	systemcnf $(TARGET).exe > cd_root/system.cnf
#	mkisofs -o $(TARGET).hsf -V MAIN -sysid PLAYSTATION cd_root
#	mkpsxiso $(TARGET).hsf $(TARGET).bin $(CDLIC_FILE)

all: $(LIB) $(MIX_LIB)
	#mkdir -p cd_root
	#$(CC) $(CFLAGS) $(SOURCES) -o $(TARGET).elf $(INCDIRS) -L./lib -lSDL -lSDL_mixer
	#elf2exe $(TARGET).elf $(TARGET).exe
	#cp $(TARGET).exe cd_root
	#cp test.txt cd_root
	#cp hello.bmp cd_root
	#cp bg.bmp cd_root
	#cp ff6.bmp cd_root
	#cp hello2.bmp cd_root
	#cp *.bmp cd_root
	#cp *.raw cd_root
	#systemcnf $(TARGET).exe > cd_root/system.cnf
	#mkisofs -o $(TARGET).hfs -V TEST -sysid PLAYSTATION cd_root
	#mkpsxiso $(TARGET).hfs $(TARGET).bin $(CDLIC_FILE)
	echo "Done building Lib!"

$(LIB): $(OBJS)
	echo "Done with objs!"
	mkdir -p lib
	$(AR) cru $(LIB) $(OBJS)

$(MIX_LIB): $(MIX_OBJS)
	echo "Done with mix objs!"
	mkdir -p lib
	$(AR) cru $(MIX_LIB) $(MIX_OBJS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $< $(INCDIRS)

#run:
#	C:\Users\bj\Documents\psx\nopsx\nopsx.exe $(TARGET).bin
#	pcsxr -cdfile $(TARGET).bin

clean:
	rm -rf *.o src/*.o mix_src/*.o *.elf *.exe *.hfs *.hsf *.bin *.cue cd_root/* lib/*
