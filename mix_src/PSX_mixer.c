/*
 * PSX_mixer.c
 */

#include "PSX_mixer.h"

typedef struct {
    Mix_Music music;
    uint32_t  data_addr; // location offset of the sound data from the SPU base address
    uint32_t  voice_num; // which voice this is attached to
} PSX_music;

typedef struct {
    Mix_Chunk chunk;
    uint32_t data_addr;
    uint32_t voice_num;
} PSX_chunk;

// keep track of the current available voice
static int used_voices = 0;

#define MAX_NUM_MUSIC 10 // arbitrary, TODO - make this a linked list or something
static PSX_music psx_music_list[MAX_NUM_MUSIC];
static int num_musics = 0; // how many structs we've used/made
static uint32_t spu_data_loc = SPU_DATA_BASE_ADDR; // current memory location to upload music to

// this will use the same spu_data_loc
static int num_chunks = 0;
static PSX_chunk psx_chunk_list[MAX_NUM_MUSIC];

static char *psx_mix_error = NULL;
static void PSX_mix_set_error(char *error_str) {
    psx_mix_error = error_str;
}

char *Mix_GetError(void) {
    return psx_mix_error;
}

int Mix_OpenAudio(int frequency, Uint16 format, int channels, int chunksize)
{
    /* Initialize PSX sound */
    SsInit();

    return 0;
}

Mix_Music *Mix_LoadMUS(const char *filename)
{
    //static uint8_t file_buffer[500000]; // static, memory allocation appeared to kill stuff
    /* Pointer to return */
    Mix_Music *return_mus = &(psx_music_list[num_musics].music);

    /* To make sure we read all of the file */
    int bytes_read;

    /* Holds the given file */
    uint8_t *file_buffer = NULL;
    uint32_t file_size;
    FILE *fp = fopen(filename, "rb");
    if (!fp) {
        PSX_mix_set_error("Error opening file");
        return NULL;
    }

    /* Set a new struct entry */
    psx_music_list[num_musics].data_addr = spu_data_loc;
    psx_music_list[num_musics].voice_num = used_voices;

    /* TODO - check if file is a .wav, .vag, etc... 
     * Right now we're assuming the data is a .raw file */
    // if (strcmp()) ...

    /* See how much space we'll need to hold the file */
    fseek(fp, 0, SEEK_END);
    file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    /* Allocate memory for the file */
    file_buffer = (uint8_t*)malloc(file_size);
    if (!file_buffer) {
        PSX_mix_set_error("Error allocating memory for file buffer");
        fclose(fp);
        return NULL;
    }

    /* Copy the file into the buffer */
    bytes_read = fread(file_buffer, 1, file_size, fp);
    if (bytes_read != file_size) {
        PSX_mix_set_error("Failed to read appropriate file size");
        fclose(fp);
        free(file_buffer);
        return NULL;
    }

    /* Upload the RAW data to the SPU */
    SsUpload(file_buffer, file_size, spu_data_loc);

    /* Set SPU properties */
    SsVoiceStartAddr(used_voices, spu_data_loc);
    SsVoiceVol(used_voices, 0x3FFF, 0x3FFF);
    SsVoicePitch(used_voices, 0x1000 / (44100 / 11025)); // I'm not sure what these numbers mean...

    /* Increment where we upload data to the SPU 
     * This is copied from the psxsnake example; assuming it has to do with
     * data alignment 
     */
    if (file_size & 0x7)
    {
        file_size |= 0x7;
        file_size++;
    }
    spu_data_loc += file_size;

    /* Increase the number of musics uploaded */
    ++num_musics;
    ++used_voices;

    /* Free our memory */
    free(file_buffer);

    /* Close da file */
    fclose(fp);

    /* Return the pointer to the matching mix music in our struct */
    return return_mus;
}

int Mix_PlayMusic(Mix_Music *music, int loops)
{
    int i;
    int psx_mus_num = -1;

    /* Figure out which entry we're using */
    for (i = 0; i < num_musics; ++i) {
        if (music == &psx_music_list[i].music) {
            psx_mus_num = i;
            break;
        }
    }

    /* Error check */
    if (psx_mus_num == -1) {
        PSX_mix_set_error("Failed to find music in music list");
        return -1;
    }

    /* Play the music at that entry */
    SsKeyOn(psx_music_list[i].voice_num);
    printf("Played music num %d\n", psx_mus_num);

    return 0;
}

void Mix_PauseMusic(void)
{
    int i;
    for (i=0; i<num_musics; ++i) {
        /* turn off the channel for that music */
        SsKeyOff(psx_music_list[i].voice_num);
        printf("Stopped music num %d\n", psx_music_list[i].voice_num);
    }
    
    return 0;
}

int Mix_HaltMusic(void)
{
    int i;
    printf("In halt music!\n");
    for (i=0; i<num_musics; ++i) {
        /* turn off the channel for that music */
        SsKeyOff(psx_music_list[i].voice_num);
        printf("Stopped music num %d\n", psx_music_list[i].voice_num);
    }
    
    return 0;
}

Mix_Chunk *Mix_LoadWAV(char *file)
{
//static uint8_t file_buffer[500000]; // static, memory allocation appeared to kill stuff
    /* Pointer to return */
    Mix_Chunk *return_chunk = &(psx_chunk_list[num_chunks].chunk);

    /* To make sure we read all of the file */
    int bytes_read;

    /* Holds the given file */
    uint8_t *file_buffer = NULL;
    uint32_t file_size;
    FILE *fp = fopen(file, "rb");
    if (!fp) {
        PSX_mix_set_error("Error opening file");
        return NULL;
    }

    /* Set a new struct entry */
    psx_chunk_list[num_chunks].data_addr = spu_data_loc;
    psx_chunk_list[num_chunks].voice_num = used_voices;

    /* TODO - check if file is a .wav, .vag, etc... 
     * Right now we're assuming the data is a .raw file */
    // if (strcmp()) ...

    /* See how much space we'll need to hold the file */
    fseek(fp, 0, SEEK_END);
    file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    /* Allocate memory for the file */
    file_buffer = (uint8_t*)malloc(file_size);
    if (!file_buffer) {
        PSX_mix_set_error("Error allocating memory for file buffer");
        fclose(fp);
        return NULL;
    }

    /* Copy the file into the buffer */
    bytes_read = fread(file_buffer, 1, file_size, fp);
    if (bytes_read != file_size) {
        PSX_mix_set_error("Failed to read appropriate file size");
        fclose(fp);
        free(file_buffer);
        return NULL;
    }

    /* Upload the RAW data to the SPU */
    SsUpload(file_buffer, file_size, spu_data_loc);

    /* Set SPU properties */
    SsVoiceStartAddr(used_voices, spu_data_loc);
    SsVoiceVol(used_voices, 0x3FFF, 0x3FFF);
    SsVoicePitch(used_voices, 0x1000 / (44100 / 11025)); // I'm not sure what these numbers mean...

    /* Increment where we upload data to the SPU 
     * This is copied from the psxsnake example; assuming it has to do with
     * data alignment 
     */
    if (file_size & 0x7)
    {
        file_size |= 0x7;
        file_size++;
    }
    spu_data_loc += file_size;

    /* Increase the number of musics uploaded */
    ++num_chunks;
    ++used_voices;

    /* Free our memory */
    free(file_buffer);

    /* Close da file */
    fclose(fp);

    /* Return the pointer to the matching mix music in our struct */
    return return_chunk;
}

int Mix_PlayChannel(int channel, Mix_Chunk *chunk, int loops)
{
    int i;
    int psx_mus_num = -1;

    /* Figure out which entry we're using */
    for (i = 0; i < num_chunks; ++i) {
        if (chunk == &psx_chunk_list[i].chunk) {
            psx_mus_num = i;
            break;
        }
    }

    /* Error check */
    if (psx_mus_num == -1) {
        PSX_mix_set_error("Failed to find music in music list");
        return -1;
    }

    /* Play the music at that entry */
    SsKeyOn(psx_chunk_list[i].voice_num);
    printf("Played chunk num %d\n", psx_mus_num);

    return 0;
}

